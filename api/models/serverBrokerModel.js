'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ServerSchema = new Schema({
  brokerMatchId: {
    type: String,
    required: 'Enter a unique match ID'
  },
  serverName: {
    type: String,
    required: 'Enter the name of the server'
  },
  playerCount: {
    type: Number,
    default: 0
  },
  port: {
    type: Number,
    default: 10000
  },
  private: {
    type: Boolean
  },
  serverPassword: {
    type: String
  },
  containerId: {
    type: String,
    required: 'Enter the id of the server container'
  },
  playerId: {
    type: String
  },
  updateToken: {
    type: String
  },
  gameStatus: {
    type: String,
    default: "waiting"
  },
  maxPlayers: {
    type: Number,
    default: 0
  }
});

module.exports = mongoose.model('Servers', ServerSchema);
