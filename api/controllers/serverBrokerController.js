'use strict';

//import { v4 as uuidv4 } from 'uuid';
const uuidv4 = require('uuid/v4');

// imports the dockerode modul to the docker variable
var Docker = require("dockerode");
// imports the mongoose modul to the docker variable
var mongoose = require('mongoose'),
Server = mongoose.model('Servers');

var request = require('request');

const dockerHost = "v1.24";
const dockerPort = 80;   //2375;
const dockerImage = "critically-entangled-server";
const gameServerPort = 11234;

// creates a new docker Object with version 1.24 (dockerHost var ) and on port 80 (dockerport var)
var docker = new Docker({
  socketPath: '/var/run/docker.sock',
  host: dockerHost,
  port: dockerPort
});

var safeServerFields = "brokerMatchId serverName maxPlayers port private gameStatus playerCount"; // fields that are safe to return to client


// cleans out servers
exports.list_all_servers = async function(req, res) {
  // creates var server with the .find() method    durchsucht find() nicht arrays ?
  let servers = await Server.find({})
  console.log({servers: servers});
  // looping throug server array and log the container to the console
  for (let i = 0; i < servers.length; i++) {
    let server = servers[i];
    let container = await docker.getContainer(server.containerId);
    // console.log({container: container});

    let containerInfo = null;
    try {
      containerInfo = await container.inspect();
    } catch (error) {
      //throw new Error("Container doesn't exist");
    }
    // deleting server from the array if container info = null  or the container status is exited and logs the deleted server to the console
    //console.log({msg: "containerInfo", containerInfo: containerInfo});
    if (containerInfo == null || containerInfo.State.Status == "exited") {
      console.log({msg: "Deleting non-existent server from DB", server: server._id});
      let deleted = await Server.deleteOne(server);
      console.log({msg: "deleted", deleted: deleted});
    }
    //deletes the container or server  if the container info is not null and container status is exited
    if (containerInfo != null && containerInfo.State.Status == "exited") {
      console.log({msg: "Removing container", containerInfo: containerInfo.Id});
      let removed = await container.remove();
      console.log({msg: "removed", removed: removed});
    }
  }
  // creates an array with the cleaned serverlist after the list all server function
  let cleanedServers = await Server.find({gameStatus: "waiting", maxPlayers: { $gt: 1 }}, safeServerFields)
  console.log({msg: "cleanedServers", cleanedServers: cleanedServers});

  res.json(cleanedServers);
};

// create_a_server function creates a new server with a randomly port over 11001
exports.create_a_server = async function(req, res) {
  console.log(req.body);

  let serverCount = await countServers();
  if (serverCount >= process.env.MAX_NUM_GAME_SERVERS) {
    console.log({serverCount: serverCount, MAX_NUM_GAME_SERVERS: process.env.MAX_NUM_GAME_SERVERS})
    res.status(507).json({error: 1, msg: "Max number of game servers exceeded. Please try again in a few minutes."});
    return;
  }

  var playerAuthId = req.body.playerAuthId;
  var playerAuthToken = req.body.playerAuthToken;

  console.log({authServerAddress: process.env.AUTH_SERVER_ADDRESS});
  var authQuery = process.env.AUTH_SERVER_ADDRESS+"/tokenCheck/"+playerAuthId+"/"+playerAuthToken;
  console.log({authQuery: authQuery});

  request(authQuery, async function (authReqError, authResponse, authBody) {
      console.log({authReqError: authReqError});
      if (authReqError != null) {
        console.log("Error during auth request");
        res.status(401).json({error: 1, msg: "Error during auth request"});
        return;
      }

      console.log({statusCode: authResponse.statusCode});
      authBody = JSON.parse(authBody);
      console.log({authBody: authBody});

      // check if player is authorized to start a server
      if (authReqError == null && authBody.error == 0 && authResponse.statusCode == 200) {
          console.log("Player "+playerAuthId+" successfully authorized by auth server, with token: "+playerAuthToken);

          // check if player already has a server running
          var playerAlreadyRunServer = await playerServerCheck(playerAuthId);
          console.log({playerAlreadyRunServer: playerAlreadyRunServer});

          if (playerAlreadyRunServer == true) {
            console.log("Player server request denied, because already running a server");
            res.status(401).json({error: 1, msg: "You already have a server running"});
            return;
          }
          else {
            console.log('Player ' +playerAuthId + ' alowed to create a server' );
          };

          // fill server fields from request
          var new_server = new Server(req.body);

          // find port for server
          let unusedPort = await findUnusedPort();
          if (unusedPort == null) {
            res.status(507).json({error: 1, msg: "Could not find a free port for server"});
            return;
          }
          new_server.port = unusedPort;
          // logs the new server to the console
          console.log("Create server: " + new_server.serverName);

          // generate unique server broker id
          new_server.brokerMatchId = uuidv4();
          // generate unique updateToken
          new_server.updateToken = uuidv4();
          new_server.gameStatus = "waiting";

          // check if server is private
          new_server.private = false
          if (new_server.serverPassword != null && new_server.serverPassword != "") {
            new_server.private = true;
          }

          // creates a new docker container
          docker.createContainer({
                Image: dockerImage,
                // name: new_server.serverName,
                ExposedPorts: {
                  [gameServerPort+"/udp"]: {}
                },
                HostConfig: {
                  PortBindings: {
                    [gameServerPort+"/udp"]: [{
                      HostPort: new_server.port+"/udp"
                    }]
                  },
                  Binds: [
                    '/var/game-server-logs/:/var/game-server-logs/:rw',
                    '/var/game-recordings/:/var/game-recordings/:rw'
                  ],
                  NetworkMode: "critically-entangled-net",
                  AutoRemove: true
                },
                // Env: [
                //   "VAR=value",
                // ],
                Cmd: [
                  "script",
                  "--command",
                  "/var/srv/godot/CE_launcher.64"
                    +" --configFile="+process.env.GAME_CONFIG_FILE
                    +" --dedicatedServer=true"
                    +" --maxPlayers="+new_server.maxPlayers
                    +" --serverPassword="+new_server.serverPassword
                    +" --serverUpdateToken="+new_server.updateToken
                    +" --serverBrokerMatchId="+new_server.brokerMatchId
                    ,
                  "--return",
                  "--flush",
                  "/var/game-server-logs/match_"+new_server.brokerMatchId+".log"
                ]
             },
             //function to catch errors
             function (err, container) {
               if (err) {
                 throw (err);
               }
               //function to catch errors when container starts
               container.start(function (err, data) {
                  if (err) {
                     throw (err);
                  }
                  // logs starting to console and ckecks for errors
                  console.log("Starting container");
                  container.inspect(function (err, data) {
                     if (err) {
                        throw (err);
                      }
                        // if no errors logs conainer starting to console
                      console.log("container running: ");
                      //console.log(data);
                      //cb(dockerHost + ":" + data.NetworkSettings.Ports['8080/tcp'][0].HostPort);


                      // save the server to DB if the request to Docker API was successful
                      new_server.containerId = data.Id;
                      new_server.playerId = playerAuthId;
                      new_server.save(function(err, savedServer) {
                        if (err) {
                          res.send(err);
                        }
                        console.log("saved to DB");

                        // remove unnecessary fields from sevrer before sending to response
                        var wantedKeys = safeServerFields.split(" ");
                        var cleanedServer = {};
                        wantedKeys.forEach(key => cleanedServer[key] = savedServer[key]);
                        console.log({"cleanedServer": cleanedServer});

                        res.json(cleanedServer);
                      });

                 });
               });
             }
           );
       }
       else {
         res.status(401).json({error: 1, msg: "Not authorized"});
         console.log("Player "+playerAuthId+" not authorized by auth server, with token: "+playerAuthToken);
       }
  })

};

// finds server by id and sends back an error if there an error
exports.read_a_server = function(req, res) {
  Server.findById(req.params.serverId, function(err, server) {
    if (err)
      res.send(err);
    res.json(server);
  });
};

// removes server
exports.delete_a_server = function(req, res) {
  Server.remove({
    _id: req.params.serverId
  }, function(err, server) {
    if (err)
      res.send(err);
    res.json({ message: 'Server successfully deleted' });
  });
};

// checks if player already runs a server
var playerServerCheck = async function (playerId) {
    let outerServer = await Server.findOne({playerId: playerId, gameStatus: { $in: ["waiting", "started"]}}, function(err, server){
      console.log('server =' + server)
      if (err) {
        return done(err);
      }
    });

    if (outerServer == null) {
      return false
    }
    else {
      return true
    }
  }

var findUnusedPort = async function() {
  let runningServers = await Server.find({}, "port", function(err, servers) {
    if (err) {
      return done(err);
    }
  });
  console.log('used ports: ' + runningServers);

  let foundUnusedPort = null;
  for (let i = 0; foundUnusedPort == null && i < 50; i++) { // prevent endless loop
    let testingPort = 11001 + Math.floor(Math.random() * Math.floor(99));

    let usedPorts = runningServers.filter(function (server) {
      return server.port === testingPort;
    });

    if (usedPorts.length == 0) {
      foundUnusedPort = testingPort;
    }
  }

  return foundUnusedPort;
}

var countServers = async function() {
  let serverCount = await Server.count({});
  console.log('server count: ' + serverCount);
  return serverCount;
}

exports.update_server_info = function(req, res) {
  console.log({function: "update_server_info", "req.params.serverId": req.params.serverId, "req.body": req.body});

  Server.findOne({"brokerMatchId": req.params.serverId}, function(err, server) {

    console.log({server: server});

    if (err || server == null) {
      console.log('nope')
      console.log(err)
      res.send(err)
    }
    else if (server.updateToken === req.body.updateToken) {
      console.log('found server to update:' + server)
      server.playerCount = req.body.playerCount;
      server.gameStatus = req.body.gameStatus;
      console.log('server updated: ' +server)

      res.json({message: 'Server updated! ' + 'Playercount=' + server.playerCount + ' Gamestatus=' + server.gameStatus})

      server.save(function(err){
          if (err)
              throw err;
          return (null, server);
      })
    }
    else {
      console.log('updateToken not matching')
      console.log("server.updateToken "+server.updateToken)
      console.log("req.body.updateToken "+req.body.updateToken)
      res.json({message: 'updateToken not matching'})
    }
  })
}
