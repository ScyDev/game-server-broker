'use strict';
module.exports = function(app) {
  var serverBroker = require('../controllers/serverBrokerController');

  app.route('/servers')
    .get(serverBroker.list_all_servers)
    .post(serverBroker.create_a_server);


  app.route('/servers/:serverId')
    .get(serverBroker.read_a_server)
    .delete(serverBroker.delete_a_server)
    .patch(serverBroker.update_server_info);
};
